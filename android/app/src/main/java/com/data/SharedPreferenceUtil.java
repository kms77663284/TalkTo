package com.data;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferenceUtil {
    public static final String APP_SHARED_PREFS = "thisApp.SharedPreference";
    private SharedPreferences sharedPreferences;
    private Editor editor;

    public SharedPreferenceUtil(Context ctx){
        this.sharedPreferences = ctx.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
    }

    public void setSharedID(String userid){
        editor.putString("id", userid);
        editor.apply();
    }

    public void setSharedPhone(String phone){
        editor.putString("phone", phone);
        editor.apply();
    }

    public void setSharedName(String name){
        editor.putString("name", name);
        editor.apply();
    }

    public void setSharedPwd(String pwd){
        editor.putString("pwd", pwd);
        editor.apply();
    }
    public String getSharedID(){
        return sharedPreferences.getString("id", "");
    }
    public String getSharedPhone(){return sharedPreferences.getString("phone", "");}
    public String getSharedName(){return  sharedPreferences.getString("name", "");}
    public String getSharedPwd() {return sharedPreferences.getString("pwd", "");}
}
