package com.data;

import java.io.Serializable;

public class UserVO implements Serializable {
    private String ID;
    private String Phone;
    private String Name;
    public String getID() {
        return ID;
    }
    public void setID(String iD) {
        ID = iD;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String phone) {
        Phone = phone;
    }
    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }

}
