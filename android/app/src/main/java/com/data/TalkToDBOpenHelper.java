package com.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class TalkToDBOpenHelper extends SQLiteOpenHelper {
    private static final String DB = "talkto.db";

    public TalkToDBOpenHelper(@Nullable Context context, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DB, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataBases.ChatDB.CREATE);
        db.execSQL(DataBases.ChatRoomDB.CREATE);
        db.execSQL(DataBases.UserDB.CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("delete from " + DataBases.ChatRoomDB.TABLE);
        db.execSQL("drop table if exists "+ DataBases.ChatDB.TABLE);
        db.execSQL("drop table if exists " + DataBases.ChatRoomDB.TABLE);
        db.execSQL("drop table if exists " + DataBases.UserDB.TABLE);
        onCreate(db);
    }
}
