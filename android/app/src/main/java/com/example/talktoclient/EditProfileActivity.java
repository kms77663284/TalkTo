package com.example.talktoclient;

import android.Manifest;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView backimg;              //배경 이미지
    private ImageView profileimg;           //프로필 이미지
    private EditText editusername;          //이름 Text
    private EditText editSmsg;               //상태 메세지 Text
    private EditText editbackimg;           //배경이미지 Text
    private ImageButton backbtn;             //뒤로가기 버튼
    private ImageButton pfPtbtn;             // 프로필 사진 편집 버튼
    private ImageButton editbackimgbtn;     //배경이미지 편집 버튼
    private ImageButton editnamebtn;        //이름 편집 버튼
    private ImageButton editSmsgbtn;        //상태메세지 편집 버튼
    private Button editSuc;                 //완료 버튼 -> 프로필 메인으로 이동
    private Uri photoUri, bakcUri;          // 프로필 사진, 배경사진 Uri


    private String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private static final int MULTIPLE_PERMISSIONS = 101;

    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_ALBUM = 1;
    private static final int CROP_FROM_CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        checkPermissions();
        initView();
    }


    private void initView() {
        backbtn = (ImageButton) findViewById(R.id.backbtn);
        editusername = (EditText) findViewById(R.id.editusername);
        editnamebtn = (ImageButton) findViewById(R.id.editnamebtn);
        editSmsg = (EditText) findViewById(R.id.editSmsg);
        editSmsgbtn = (ImageButton) findViewById(R.id.editSmsgbtn);
        editSuc = (Button) findViewById(R.id.editSuc);
        pfPtbtn = (ImageButton) findViewById(R.id.pfPtbtn);
        profileimg = (ImageView) findViewById(R.id.profileimg);
        backimg = (ImageView) findViewById(R.id.backimg);
        editbackimgbtn = (ImageButton) findViewById(R.id.editbackimgbtn);

        backbtn.setOnClickListener(this);
        editnamebtn.setOnClickListener(this);
        editSmsgbtn.setOnClickListener(this);
        editSuc.setOnClickListener(this);
        pfPtbtn.setOnClickListener(this);
        editbackimgbtn.setOnClickListener(this);


        Intent intent = getIntent();
        String upName = intent.getStringExtra("upName");
        String upSmsg = intent.getStringExtra("upSmsg");
        Uri photoUri = intent.getParcelableExtra("photoUri");
        Uri bakcUri = intent.getParcelableExtra("bakcUri");

        editusername.setText(upName);
        editSmsg.setText(upSmsg);
        profileimg.setImageURI(photoUri);
        backimg.setImageURI(bakcUri);

    }
    private boolean checkPermissions() {
        int result;
        List<String> permissionList = new ArrayList<>();
        for (String pm : permissions) {
            result = ContextCompat.checkSelfPermission(this, pm);
            if (result != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(pm);
            }
        }
        if (!permissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissionList.toArray(new String[permissionList.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public void doTakePhotoActionProfile() {
        // 카메라 앱을 여는 소스
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFileProfile();
        } catch (IOException e) {
            Toast.makeText(EditProfileActivity.this, "이미지 처리 오류! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            //finish();
            e.printStackTrace();
        }
        if (photoFile != null) {
            photoUri = FileProvider.getUriForFile(EditProfileActivity.this,
                    "com.example.provider", photoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(intent, PICK_FROM_CAMERA);
        }
    }

    private File createImageFileProfile() throws IOException {
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "TalkToProfile" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/TalkTo/");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        return image;
    }

    public void doTakeAlbumActionProfile() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }


    public void doTakePhotoActionBackimg() {
        // 카메라 앱을 여는 소스
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFileProfile();
        } catch (IOException e) {
            Toast.makeText(EditProfileActivity.this, "이미지 처리 오류! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            //finish();
            e.printStackTrace();
        }
        if (photoFile != null) {
            bakcUri = FileProvider.getUriForFile(EditProfileActivity.this,
                    "com.example.provider", photoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, bakcUri);
            startActivityForResult(intent, PICK_FROM_CAMERA);
        }
    }

    private File createImageFileBackimg() throws IOException {
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "TalkToBackImg" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/TalkTo/");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        return image;
    }

    public void doTakeAlbumActionBackimg() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    // 버튼 onClick리스너 처리부분
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backbtn:
                // 뒤로가기 버튼
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.editnamebtn:
                intent = new Intent(getApplicationContext(), PopupActivity.class);
                intent.putExtra("upName", editSmsg.getText().toString());
                startActivity(intent);
                finish();
                break;
            case R.id.editSmsgbtn:
                intent = new Intent(getApplicationContext(), PopupActivity2.class);
                intent.putExtra("Stmsg", editSmsg.getText().toString());
                startActivity(intent);
                finish();
                break;
            case R.id.editSuc:
                intent = new Intent(getApplicationContext(), ProfileActivity.class);
                intent.putExtra("upSmsg", editSmsg.getText().toString());
                intent.putExtra("upName", editusername.getText().toString());
                intent.putExtra("photoUri", photoUri);
                intent.putExtra("bakcUri", bakcUri);
                startActivity(intent);
                finish();
                break;
            case R.id.pfPtbtn:
                DialogInterface.OnClickListener cameraListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doTakePhotoActionProfile();
                    }
                };
                DialogInterface.OnClickListener albumListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doTakeAlbumActionProfile();
                    }
                };
                DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
                new AlertDialog.Builder(this).setTitle("업로드할 이미지 선택")
                        .setPositiveButton("사진촬영", cameraListener)
                        .setNeutralButton("앨범선택", albumListener)
                        .setNegativeButton("취소", cancelListener)
                        .show();
                break;
            case R.id.editbackimgbtn:
                cameraListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doTakePhotoActionBackimg();
                    }
                };
                albumListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doTakeAlbumActionBackimg();
                    }
                };
                cancelListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
                new AlertDialog.Builder(this).setTitle("업로드할 이미지 선택")
                        .setPositiveButton("사진촬영", cameraListener)
                        .setNeutralButton("앨범선택", albumListener)
                        .setNegativeButton("취소", cancelListener)
                        .show();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++) {
                        if (permissions[i].equals(this.permissions[0])) {
                            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                                showNoPermissionToastAndFinish();
                            }
                        } else if (permissions[i].equals(this.permissions[1])) {
                            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                                showNoPermissionToastAndFinish();

                            }
                        } else if (permissions[i].equals(this.permissions[2])) {
                            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                                showNoPermissionToastAndFinish();

                            }
                        }
                    }
                } else {
                    showNoPermissionToastAndFinish();
                }
                return;
            }
        }
    }

    private void showNoPermissionToastAndFinish() {
        Toast.makeText(this, "권한 요청에 동의 해주셔야 이용 가능합니다. 설정에서 권한 허용 하시기 바랍니다.", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void cropImage() {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(photoUri, "image/*");
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        grantUriPermission(list.get(0).activityInfo.packageName, photoUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
            return;
        } else {
            Toast.makeText(getApplicationContext(), "용량이 큰 사진의 경우 시간이 오래 걸릴 수 있습니다.", Toast.LENGTH_SHORT).show();
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);

            File croppedFileName = null;
            try {
                croppedFileName = createImageFileProfile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            File folder = new File(Environment.getExternalStorageDirectory() + "/TalkTo/");
            File tempFile = new File(folder.toString(), croppedFileName.getName());
            photoUri = FileProvider.getUriForFile(EditProfileActivity.this,
                    "com.example.provider", tempFile);
            intent.putExtra("return-data", false);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

            Intent i = new Intent(intent);
            ResolveInfo res = list.get(0);
            grantUriPermission(res.activityInfo.packageName, photoUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            startActivityForResult(i, CROP_FROM_CAMERA);

        }
    }

    public void cropImageback() {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(bakcUri, "image/*");
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        grantUriPermission(list.get(0).activityInfo.packageName, bakcUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
            return;
        } else {
            Toast.makeText(getApplicationContext(), "용량이 큰 사진의 경우 시간이 오래 걸릴 수 있습니다.", Toast.LENGTH_SHORT).show();
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 2);
            intent.putExtra("scale", true);

            File croppedFileName = null;
            try {
                croppedFileName = createImageFileBackimg();
            } catch (IOException e) {
                e.printStackTrace();
            }
            File folder = new File(Environment.getExternalStorageDirectory() + "/TalkTo/");
            File tempFile = new File(folder.toString(), croppedFileName.getName());
            bakcUri = FileProvider.getUriForFile(EditProfileActivity.this,
                    "com.example.provider", tempFile);
            intent.putExtra("return-data", false);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, bakcUri);
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

            Intent i = new Intent(intent);
            ResolveInfo res = list.get(0);
            grantUriPermission(res.activityInfo.packageName, bakcUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            startActivityForResult(i, CROP_FROM_CAMERA);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            Toast.makeText(this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (requestCode == PICK_FROM_ALBUM) {
            if (intent == null) {
                return;
            }
            photoUri = intent.getData();
            cropImage();
        } else if (requestCode == PICK_FROM_CAMERA) {
            cropImage();
            // 갤러리에 나타나게
            MediaScannerConnection.scanFile(EditProfileActivity.this,
                    new String[]{photoUri.getPath()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                        }
                    });
        } else if (requestCode == CROP_FROM_CAMERA) {
            profileimg.setImageURI(null);
            profileimg.setImageURI(photoUri);
            revokeUriPermission(photoUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }

//        //back
//        if (resultCode != RESULT_OK) {
//            Toast.makeText(this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if (requestCode == PICK_FROM_ALBUM) {
//            if (intent == null) {
//                return;
//            }
//            bakcUri = intent.getData();
//            cropImageback();
//        } else if (requestCode == PICK_FROM_CAMERA) {
//            cropImageback();
//            // 갤러리에 나타나게
//            MediaScannerConnection.scanFile(EditProfileActivity.this,
//                    new String[]{bakcUri.getPath()}, null,
//                    new MediaScannerConnection.OnScanCompletedListener() {
//                        public void onScanCompleted(String path, Uri uri) {
//                        }
//                    });
//        } else if (requestCode == CROP_FROM_CAMERA) {
//            profileimg.setImageURI(null);
//            profileimg.setImageURI(bakcUri);
//            revokeUriPermission(bakcUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//        }

    }
}