package com.example.talktoclient;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class PopupActivity2 extends AppCompatActivity {
    private Button okbtn;
    private EditText upSmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_popup2);

        okbtn = (Button)findViewById(R.id.okbtn);
        upSmsg = (EditText)findViewById(R.id.upSmsg);

        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),EditProfileActivity.class);
                intent.putExtra("upSmsg",upSmsg.getText().toString());
                startActivity(intent);
                finish();
            }
        });
    }
}
