package com.example.talktoclient;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.data.DataBases;
import com.data.FriendListVO;
import com.data.SharedPreferenceUtil;
import com.data.TalkToDBOpenHelper;
import com.data.UserVO;
import com.server.ServerData;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {
    private static final String URL="http://"+ ServerData.IP+":"+ServerData.JSPPORT;

    private ImageView backimg;      //배경이미지
    private ImageView profileimg;   //프로필 이미지
    private ImageButton pfclosebtn; //닫기 버튼
    private TextView Smsg;          //상태메세지 Text
    private TextView username;      //이름 Text
    private Button selfChatbtn;     //나와의 채팅 버튼
    private Button pfEditbtn;       // 프로필 편집 버튼

    private SharedPreferenceUtil session;
    private ClientService cs;
    boolean isService = false;

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ClientService.ClientBinder cb = (ClientService.ClientBinder)service;
            cs = cb.getService();
            isService = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isService = false;
        }
    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int roomid = intent.getIntExtra("roomnum", -1);
            if (roomid == -1){
                Toast.makeText(context,"채팅방 생성에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
            else {
                Intent intochat = new Intent(ProfileActivity.this, ChatActivity.class);
                intochat.putExtra("roomid", roomid);
                startActivity(intochat);
                finish();
            }
        }
    };

    private FriendListVO f_info; //친구 (한명)정보
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_profile);

        session = new SharedPreferenceUtil(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //친구리스트에서 인텐트로 정보를 넘겨받는다
        Intent revIntent = getIntent();
        f_info = new FriendListVO();
        addfriendInfo(revIntent);//f_info에 정보 넣어주는메소드

        //프로필 편집 버튼 클릭 -> 프로필 편집 화면으로 이동
        pfEditbtn = (Button)findViewById(R.id.pfEditbtn);
        pfEditbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),EditProfileActivity.class);
                startActivity(intent);

            }
        });
        //EditProfileActivity.java 원하는 이름 입력 후 편집 버튼 클릭시
        //프로필 창 이름 바뀜
        username = (TextView)findViewById(R.id.username);

        username.setText(f_info.getUser_name());

        //EditProfileActivity.java 원하는 상태 메세지 입력 후 편집 버튼 클릭시
        //프로필 창 상태메세지 바뀜
        Smsg = (TextView)findViewById(R.id.Smsg);

        Smsg.setText(f_info.getMessage());

        //EditProfileActivity.java 원하는 프로필 사진 선택 후 편집 버튼 클릭시
        //프로필 창 프로필사진 바뀜
        selfChatbtn = (Button)findViewById(R.id.selfChatbtn);
        selfChatbtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (!checkExist()) cs.sendCreateChatRoom(new ArrayList<UserVO>(), false);
            }
        });
        profileimg = (ImageView)findViewById(R.id.profileimg);
        Glide.with(this).load(URL+f_info.getProfileaddress()).into(profileimg);
        backimg = (ImageView)findViewById(R.id.backimg);
        Glide.with(this).load(URL+f_info.getBackgaddress()).into(backimg);
       // backimg.setImageResource(R.drawable.backtest2);

        Intent bindservice = new Intent(this, ClientService.class);
        bindService(bindservice, conn, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, new IntentFilter("selfprofilechat"));
    }
    public void addfriendInfo(Intent intent){
        Log.d("LOG","addfriendInfo에서 f_info에 정보넣음");
        f_info.setState(intent.getStringExtra("state"));
        f_info.setId(intent.getStringExtra("id"));
        f_info.setUser_name(intent.getStringExtra("user_name"));
        f_info.setProfileaddress(intent.getStringExtra("profileaddress"));
        f_info.setMessage(intent.getStringExtra("message"));
        f_info.setPhoneNum(intent.getStringExtra("phoneNum"));
        f_info.setBackgaddress(intent.getStringExtra("backgaddress"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(conn);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    public boolean checkExist(){
        //ChatRoomDBHelper crdb = new ChatRoomDBHelper(getApplicationContext(), "talkto.db", null, 1);
        TalkToDBOpenHelper tdb = new TalkToDBOpenHelper(getApplicationContext(), null, 1);
        SQLiteDatabase roomdb = tdb.getWritableDatabase();
        String sql = "select " + DataBases.ChatRoomDB.RN + " from " + DataBases.ChatRoomDB.TABLE + " where " + DataBases.ChatRoomDB.USERS + "=?";
        Cursor cur =  roomdb.rawQuery(sql, new String[]{session.getSharedID()});
        if (cur.moveToNext()){
            Intent intochat = new Intent(ProfileActivity.this, ChatActivity.class);
            intochat.putExtra("roomid", cur.getInt(cur.getColumnIndex(DataBases.ChatRoomDB.RN)));
            startActivity(intochat);
            return true;
        }
        return false;
    }
}
