package com.example.talktoclient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import com.data.DataBases;
import com.data.TalkToDBOpenHelper;
import com.data.FriendListVO;
import com.data.SharedPreferenceUtil;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.ionicons_typeface_library.Ionicons;
import com.server.ServerData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import devlight.io.library.ntb.NavigationTabBar;

public class ClientActivity extends AppCompatActivity {
    private static final String  URL = "http://"+ ServerData.IP+":"+ServerData.JSPPORT+"/TalkTo/friend.jsp";
    @BindView(R.id.ntb_horizontal)
    NavigationTabBar navTab;
    private ViewPager viewPager;
    private MainTabPagerAdapter viewPagerAdapter;
    private Intent intent;
    private List<FriendListVO> friendList;
    private FriendListVO loginInfo;
    private SharedPreferenceUtil session;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("LOG","onStart실행");
        ButterKnife.bind (this);
        initUI();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        intent = getIntent();
        Log.d("LOG","intent로 받아서 loginInfo에 넣어준다. id:"+intent.getStringExtra("id"));
        loginInfo=new FriendListVO();
        loginInfo.setId(intent.getStringExtra("id"));
        loginInfo.setBackgaddress(intent.getStringExtra("backgaddress"));
        loginInfo.setMessage(intent.getStringExtra("message"));
        loginInfo.setPhoneNum(intent.getStringExtra("phoneNum"));
        loginInfo.setProfileaddress(intent.getStringExtra("profileaddress"));
        loginInfo.setUser_name(intent.getStringExtra("user_name"));

        SharedPreferenceUtil session = new SharedPreferenceUtil(this);
        session.setSharedName(loginInfo.getUser_name());
        session.setSharedPhone(loginInfo.getPhoneNum());

        ContentValues values= new ContentValues();
        values.put("msg","friend");
        values.put("id",intent.getStringExtra("id"));

        Log.d("LOG","친구조회 Task 만든다.");
        NetworkTask networkTask = new NetworkTask(ClientActivity.this,URL, values);
        networkTask.execute();
        try{
          Thread.sleep(100); //잠시 시간차를 줘서 서버에서 값을 받아오는걸 기다린다.
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        friendList = networkTask.getFriendList();

        TalkToDBOpenHelper tdb = new TalkToDBOpenHelper(getApplicationContext(), null, 1);
        SQLiteDatabase talkdb = tdb.getWritableDatabase();
        for (FriendListVO tmp : friendList){
            ContentValues content = new ContentValues();
            content.put(DataBases.UserDB.ID, tmp.getId());
            content.put(DataBases.UserDB.ADDR, tmp.getBackgaddress());
            content.put(DataBases.UserDB.MSG, tmp.getMessage());
            content.put(DataBases.UserDB.NAME, tmp.getUser_name());
            content.put(DataBases.UserDB.PHONE, tmp.getPhoneNum());
            content.put(DataBases.UserDB.PADDR, tmp.getProfileaddress());
            talkdb.insertWithOnConflict(DataBases.UserDB.TABLE, null, content, SQLiteDatabase.CONFLICT_REPLACE);
        }
    }

    private void initUI(){
        viewPager = findViewById(R.id.vp_horizontal_ntb);
        viewPagerAdapter = new MainTabPagerAdapter(getSupportFragmentManager(),3);
        initPages();
        viewPager.setAdapter(viewPagerAdapter);
        initNavigationBar();
    }
    private void initPages() {
        //Log.d("LOG",friendList.get(0).getUser_name()+"클라이언트액티비티");
        viewPagerAdapter.addFragment( new FragmentFriend(loginInfo,friendList));
        viewPagerAdapter.addFragment(new FragmentChatting(this,friendList));
        viewPagerAdapter.addFragment(new FragmentSetting());
    }
    private void initNavigationBar(){

        int activeColor = getResources().getColor(R.color.tab_active,null);
        int inactiveColor = getResources().getColor(R.color.tab_inactive,null);

        navTab.setBgColor(Color.WHITE);
        navTab.setActiveColor(activeColor);
        navTab.setInactiveColor(inactiveColor);
        navTab.setIsTitled(true);

        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();

        Drawable ifriend = new IconicsDrawable(this, Ionicons.Icon.ion_ios_people).sizeDp(24);
        Drawable ichating =new IconicsDrawable(this, Ionicons.Icon.ion_chatbubble).sizeDp(24);
        Drawable iset =new IconicsDrawable(this, Ionicons.Icon.ion_android_settings).sizeDp(24);



        String modelBgColor = "#ffffff";
        models.add(
                new NavigationTabBar.Model.Builder(
                        ifriend,
                        Color.parseColor(modelBgColor))
                        .title(getString(R.string.main_nav_first))
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        ichating,
                        Color.parseColor(modelBgColor))
                        .title(getString(R.string.main_nav_second))
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        iset,
                        Color.parseColor(modelBgColor))
                        .title(getString(R.string.main_nav_three))
                        .build()
        );




        navTab.setModels(models);
        navTab.setViewPager(viewPager, 0);

        navTab.setBehaviorEnabled(true);
        navTab.setBadgeSize(30);
        navTab.setTitleSize(50);


        navTab.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

            }

            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {

            }
        });

        navTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });
        navTab.bringToFront();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
