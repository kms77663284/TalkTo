package com.example.talktoclient;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.data.ChatListVO;
import com.data.DataBases;
import com.data.FriendListVO;
import com.data.SharedPreferenceUtil;
import com.data.TalkToDBOpenHelper;
import com.data.UserVO;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.ionicons_typeface_library.Ionicons;

import java.util.ArrayList;
import java.util.List;

public class FragmentChatting extends Fragment {

    private ListView chattingList;
    private ClientListViewAdapter adapter;
    private Activity activity;
    private SharedPreferenceUtil session;
    private ClientService cs;
    boolean isService = false;
    private SQLiteDatabase db;
    private TalkToDBOpenHelper tdb;
    private boolean sync = false;

    ChatAdd_Adapter chatAdd_adapter;
    private List<FriendListVO> friendList;
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ClientService.ClientBinder cb = (ClientService.ClientBinder)service;
            cs = cb.getService();
            isService = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isService = false;
        }
    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String s = intent.getStringExtra("sync");
            if (s == null || s.equals("") || s.equals("in")) {
                int roomid = intent.getIntExtra("roomnum", -1);
                if (roomid == -1) {
                    Toast.makeText(context, "채팅방 생성에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    if (s == null || s.equals("") || !s.equals("in")) {
                        Cursor room = db.rawQuery("select * from " + DataBases.ChatRoomDB.TABLE + " where " + DataBases.ChatRoomDB.RN + "=?", new String[]{roomid + ""});
                        if (room.moveToNext()) {
                            String[] list = room.getString(room.getColumnIndex(DataBases.ChatRoomDB.USERS)).split(",");
                            adapter.addVO(list, null, roomid);
                            adapter.notifyDataSetChanged();
                        }
                    }
                    Intent intochat = new Intent(getActivity(), ChatActivity.class);
                    intochat.putExtra("roomid", roomid);
                    startActivity(intochat);
                    onPause();
                }
            }else if (s.equals("start")) sync = false;
            else {
                sync = true;
                SyncChatRoom();
            }
        }
    };

    public FragmentChatting(Activity activity,List<FriendListVO> friendList){
        this.activity = activity;
        this.friendList = friendList;
        chatAdd_adapter = new ChatAdd_Adapter(friendList);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
        getActivity().unbindService(conn);
    }

    public void SyncChatRoom(){
        if (sync){
            String sql = "select * from " + DataBases.ChatRoomDB.TABLE;
            Cursor cur = db.rawQuery(sql, null);
            while(cur.moveToNext()){
                String[] list = cur.getString(cur.getColumnIndex(DataBases.ChatRoomDB.USERS)).split(",");
                adapter.addVO(list,null,cur.getInt(cur.getColumnIndex(DataBases.ChatRoomDB.RN)));
            }
            adapter.notifyDataSetChanged();
            cur.close();
            sync = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("채팅프래그먼트", "onCreateView 실행");
        View view= inflater.inflate(R.layout.fragment_chatting,container,false);
       initToolbar(view);
        adapter = new ClientListViewAdapter();
        session = new SharedPreferenceUtil(getContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        tdb = new TalkToDBOpenHelper(getContext(), null, 1);
        db = tdb.getWritableDatabase();
        chattingList = (ListView)view.findViewById(R.id.chattingList);
        chattingList.setAdapter(adapter);
        chattingList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int roomid = ((ChatListVO)adapter.getItem(position)).getRoomId();
                Intent intochat = new Intent(getActivity(), ChatActivity.class);
                intochat.putExtra("roomid", roomid);
                startActivity(intochat);
                onPause();
            }
        });
        Intent bindservice = new Intent(getContext(), ClientService.class);
        getActivity().bindService(bindservice, conn, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, new IntentFilter("createchat"));
        return view;
    }
    private void initToolbar(View view){
        Log.d("채팅프래그먼트", "initToolbar 실행");
        if (Build.VERSION.SDK_INT >= 21) {
            // 21 버전 이상일 때
            getActivity().getWindow().setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = view.findViewById(R.id.toolbar2);

        toolbar.setTitle("");

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        TextView title = toolbar.findViewById(R.id.title);
        title.setText("채팅");
        ImageButton toolBtn =view.findViewById(R.id.toolbarBtn1);
        Drawable img = new IconicsDrawable(view.getContext(), Ionicons.Icon.ion_android_add).sizeDp(24);
        toolBtn.setImageDrawable(img);
        toolBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                viewAlertDialog(v);
            }
        });
    }
    public void viewAlertDialog(View v){
        AlertDialog.Builder ad = new AlertDialog.Builder(v.getContext());

        //ad.setTitle("대화 상대 추가");       // 제목 설정

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.add_chat_dialog, null);
        ListView frientlist = (ListView)view.findViewById(R.id.listview_alterdialog_list);
        Log.d("LOG","frientlist 셋 어뎁터 전");
        frientlist.setAdapter(chatAdd_adapter);
        ad.setView(view);

        frientlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView f_name = (CheckedTextView)view.findViewById(R.id.f_name);
                if(f_name.isChecked()){//체크 여부 확인
                    f_name.setChecked(false);
                    chatAdd_adapter.setCheck(position,false);
                }else{//체크 안되어있음 체크해주라
                    f_name.setChecked(true);
                    chatAdd_adapter.setCheck(position,true);
                }
            }
        });

        // 확인 버튼 설정
        ad.setPositiveButton("추가", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.v("LOG", "추가버튼 클릭");

                ArrayList<UserVO> chatlist = new ArrayList<UserVO>();
                int size = chatAdd_adapter.getCount();
                for (int i=0; i < size; i++){
                    if (chatAdd_adapter.getCheck(i)){
                        FriendListVO tmp = (FriendListVO)chatAdd_adapter.getItem(i);
                        UserVO add = new UserVO();
                        add.setName(tmp.getUser_name());
                        add.setPhone(tmp.getPhoneNum());
                        add.setID(tmp.getId());
                        chatlist.add(add);
                        chatAdd_adapter.setCheck(i, false);
                    }
                }
                cs.sendCreateChatRoom(chatlist, true);
                dialog.dismiss();     //닫기
            }
        });

        // 취소 버튼 설정
        ad.setNegativeButton("닫기", new  DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.v("LOG", "닫기 버튼 클릭");
                dialog.dismiss();     //닫기
            }
        });

        ad.show();
        Log.d("LOG","ad.show() 띄움");
    }
}
