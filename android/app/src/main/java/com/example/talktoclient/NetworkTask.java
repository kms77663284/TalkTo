package com.example.talktoclient;


import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.data.FriendListVO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NetworkTask extends AsyncTask<Void, Void, String> {

    private String url;

    private ContentValues values;

    private Context context;
    private List<FriendListVO> friendList; // 친구목록 저장 리스트
    private FriendListVO loginInfo; //로그인시 로그인유저의 정보 저장변수
    ProgressDialog asyncDialog ;
    String state ="";
    String msg ="";



    public NetworkTask(Context context, String url, ContentValues values) {

        this.url = url;
        this.context = context;
        this.values = values;
        friendList = new ArrayList<FriendListVO>();
        loginInfo = new FriendListVO();

    }
    @Override
    protected void onPreExecute() {
        asyncDialog = new ProgressDialog(
                context);
        asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        asyncDialog.setMessage("로딩중입니다..");

        // show dialog
        //asyncDialog.show();
        super.onPreExecute();
    }

    @Override

    protected String doInBackground(Void... params) {

        String result; // 요청 결과를 저장할 변수.

        RequestHttpURLConnection requestHttpURLConnection = new RequestHttpURLConnection();

        // 해당 URL로 부터 결과물을 얻어온다.
        Log.d("LOG",url);
        result = requestHttpURLConnection.request(url, values);
        Log.d("LOG",result);
        JSONObject jsonmsg =null;
        JSONObject jsonstate=null;


        try {
            msg = new JSONObject(result).get("msg").toString();

            state = new JSONObject(result).get("state").toString();
            Log.d("LOG","msg,state :"+ msg + " " + state);
            if(msg.equals("friend")){
                JSONArray jarray = new JSONObject(result).getJSONArray("friend");
                for (int i = 0; i < jarray.length(); i++) {
                    FriendListVO ListVO = new FriendListVO();
                    JSONObject jObject = jarray.getJSONObject(i);

                    ListVO.setId(jObject.optString("id"));
                    ListVO.setUser_name(jObject.optString("user_name"));
                    ListVO.setPhoneNum(jObject.optString("phoneNum"));
                    ListVO.setBackgaddress(jObject.optString("backgaddress"));
                    ListVO.setMessage(jObject.optString("message"));
                    ListVO.setProfileaddress(jObject.optString("profileaddress"));
                    friendList.add(ListVO);
                    Log.d("LOG","frindList에추가"+friendList.size());
                }
            }else if(msg.equals("login")){
                JSONObject u_info = new JSONObject(result).getJSONObject("loginInfo");
                loginInfo.setId(u_info.optString("id"));
                loginInfo.setUser_name(u_info.optString("user_name"));
                loginInfo.setPhoneNum(u_info.optString("phoneNum"));
                loginInfo.setMessage(u_info.optString("message"));
                loginInfo.setProfileaddress(u_info.optString("profileaddress"));
                loginInfo.setBackgaddress(u_info.optString("backgaddress"));
                Log.d("LOG","LoginInfo에 로그인정보 추가");


            }else if(msg.equals("addFriend")&&state.equals("친구추가성공")){
                FriendListVO ListVO = new FriendListVO();
                JSONObject f_info = new JSONObject(result).getJSONObject("friendInfo");

                ListVO.setId(f_info.optString("id"));
                ListVO.setUser_name(f_info.optString("user_name"));
                ListVO.setPhoneNum(f_info.optString("phoneNum"));
                ListVO.setBackgaddress(f_info.optString("backgaddress"));
                ListVO.setMessage(f_info.optString("message"));
                ListVO.setProfileaddress(f_info.optString("profileaddress"));
                friendList.add(ListVO);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return state;

    }



    @Override

    protected void onPostExecute(String s) {

        super.onPostExecute(s);

        // doInBackground()로 부터 리턴된 값이

        // onPostExecute()의 매개변수로 넘어오므로 s를 출력한다.
        if(s.equals("로그인성공")){
            Log.d("LOG","로그인성공");
            Intent intent = new Intent(context, ClientActivity.class);
            intent.putExtra("id",loginInfo.getId());
            intent.putExtra("backgaddress",loginInfo.getBackgaddress());
            intent.putExtra("message",loginInfo.getMessage());
            intent.putExtra("phoneNum",loginInfo.getPhoneNum());
            intent.putExtra("profileaddress",loginInfo.getProfileaddress());
            intent.putExtra("user_name",loginInfo.getUser_name());
            Log.d("LOG","intent로 loginInfo 데이터 보내기");
            context.startActivity(intent);

            //  finish();
        }else if(s.equals("로그인실패")){
            Toast.makeText(context,"아이디또는 비밀번호가 틀렸습니다.",Toast.LENGTH_SHORT).show();
        }else if(s.equals("회원가입성공")){
            Toast.makeText(context,"회원가입 성공했습니다.",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(context, Login.class);
            context.startActivity(intent);
        }else if(s.equals("회원가입중복")){
            Toast.makeText(context,"아이디가 중복입니다.",Toast.LENGTH_SHORT).show();
        }else if(s.equals("친구목록조회성공")){
           // Toast.makeText(v.getContext(),"친구목록을 불러옵니다.",Toast.LENGTH_SHORT).show();

        }else if(s.equals("친구추가실패")){

            Toast.makeText(context,"아이디 또는 폰번호를 제대로 입력해주세요.",Toast.LENGTH_SHORT).show();
        }

        //asyncDialog.dismiss();
    }

    public List<FriendListVO> getFriendList(){
        Log.d("LOG","getFriendList"+friendList.size());
        return friendList;
    }
    public FriendListVO getFriend(){
        Log.d("LOG","getFriend"+friendList.size());
        return friendList.get(0);
    }
    public String getState(){

        return state;
    }
}


