package com.example.talktoclient;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.data.FriendListVO;
import com.server.ServerData;

import java.util.ArrayList;
import java.util.List;


public class ChatAdd_Adapter extends BaseAdapter {
    private static final String URL="http://"+ ServerData.IP +":"+ServerData.JSPPORT;
    List<FriendListVO> f_list = new ArrayList<FriendListVO>();
    ArrayList<Boolean> checkboxs;

    public ChatAdd_Adapter(List<FriendListVO> f_list){
        this.f_list =f_list;
        checkboxs = new ArrayList<Boolean>();
        for (int i=0; i < f_list.size(); i++) checkboxs.add(false);
    }

    public void setCheck(int position, boolean chk){
        checkboxs.set(position, chk);
    }

    public boolean getCheck(int position){
        return checkboxs.get(position);
    }

    @Override
    public int getCount() {
        return f_list.size();
    }

    @Override
    public Object getItem(int position) {
        return f_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        if(convertView == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.chatadd_item, parent, false);
        }
        Log.d("LOG","ChatAdd_Adapter의 getView");
        ImageView f_img = (ImageView)convertView.findViewById(R.id.f_img);
        CheckedTextView f_name = (CheckedTextView)convertView.findViewById(R.id.f_name);

        Glide.with(convertView).load(URL+f_list.get(pos).getProfileaddress()).into(f_img);
        f_name.setText(f_list.get(pos).getUser_name());

        return  convertView;
    }
}
