package com.example.talktoclient;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.data.ChatListVO;

import java.util.ArrayList;
import java.util.List;

public class ClientListViewAdapter extends BaseAdapter {
    private List<ChatListVO> listVO = new ArrayList<ChatListVO>();


    @Override
    public int getCount() {
        return listVO.size();
    }

    @Override
    public Object getItem(int position) {
        return listVO.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("채팅리스트뷰어뎁터", "getView() 실행");
        final int pos= position;
        final Context context = parent.getContext();
        if(convertView ==null){
            Log.d("채팅리스트뷰어뎁터", "convertView NULL(if문 들어옴)");
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.chatting_item_view,parent,false);
            Log.d("채팅리스트뷰어뎁터", "convertView 실행");
        }
//        ImageView img = (ImageView)convertView.findViewById(R.id.img);
        TextView name =(TextView)convertView.findViewById(R.id.names);
        TextView msg = (TextView)convertView.findViewById(R.id.msg);
//
        ChatListVO listViewItem = listVO.get(position);
//        img.setImageDrawable(listViewItem.getImg());
        String user="";
        for(String s : listViewItem.getNames()){
            user += s+",";
        }
        name.setText(user.substring(0,user.length()-1));
        msg.setText(listViewItem.getMsg());
/*
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,(pos+1)+"번째 리스트가 클릭되었습니다.",Toast.LENGTH_LONG).show();
            }
        });
*/
        return convertView;
    }

    public void addVO(String[] names ,String msg, int roomnum){
        Log.d("채팅리스트뷰어뎁터", "addVO() 실행");
        ChatListVO item = new ChatListVO();
        item.setRoomId(roomnum);
        item.setNames(names);
        item.setMsg(msg);
        listVO.add(item);
    }
}
