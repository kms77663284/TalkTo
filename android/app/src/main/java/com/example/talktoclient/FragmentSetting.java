package com.example.talktoclient;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

public class FragmentSetting  extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting,container,false);
        initToolbar(view);
        //여기에 내용들 작성해주면 된다.
        return view;
    }
    private void initToolbar(View view){
        if (Build.VERSION.SDK_INT >= 21) {
            // 21 버전 이상일 때
            getActivity().getWindow().setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = view.findViewById(R.id.toolbar3);
        toolbar.getId();
        toolbar.setTitle("");

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        TextView title = toolbar.findViewById(R.id.title);
        title.setText("설정");
        ImageButton toolBtn =view.findViewById(R.id.toolbarBtn1);
        toolBtn.setVisibility(view.INVISIBLE); //설정뷰에서 버튼 안보이게
    }
}
