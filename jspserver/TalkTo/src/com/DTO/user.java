package com.DTO;

public class user {
	private int idx;
	private String id;
	private String pwd;
	private String user_name;
	private String phoneNum;
	private String message;
	private String backgaddress;
	private String profileaddress;
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getBackgaddress() {
		return backgaddress;
	}
	public void setBackgaddress(String backgaddress) {
		this.backgaddress = backgaddress;
	}
	public String getProfileaddress() {
		return profileaddress;
	}
	public void setProfileaddress(String profileaddress) {
		this.profileaddress = profileaddress;
	}
	
}
