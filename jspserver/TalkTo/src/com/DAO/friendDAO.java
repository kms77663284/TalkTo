package com.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.talkto.conn.ConnectionMariaDB;

public class friendDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static friendDAO instance;
	
	private friendDAO(){}
	
	public static friendDAO getInstance() {
		if (instance == null) instance = new friendDAO();
		return instance;
	}
	
	
	public JSONObject getFriendList(String id) {
		ResultSet rs2 =null;
		//최종 완성될 JSONObject 선언(전체)
        JSONObject jsonObject = new JSONObject();
 
        //person의 JSON정보를 담을 Array 선언
        JSONArray userArray = new JSONArray();
        
		
		if(conn == null) {
			conn = ConnectionMariaDB.getConnection();
		}
		
		try {
			sql ="select * from friend where id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				sql = "select * from users where id =?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, rs.getString("f_id"));
				rs2 = pstmt.executeQuery();
				while(rs2.next()) {
					//정보 입력
			        //person의 한명 정보가 들어갈 JSONObject 선언
					JSONObject userInfo = new JSONObject();
			        userInfo.put("id", rs2.getString("id"));
			        userInfo.put("user_name", rs2.getString("user_name"));
			        userInfo.put("phoneNum", rs2.getString("phoneNum"));
			        userInfo.put("message", rs2.getString("message"));
			        userInfo.put("backgaddress", rs2.getString("backgaddress"));
			        userInfo.put("profileaddress", rs2.getString("profileaddress"));

			        //Array에 입력
			        userArray.add(userInfo);
				}
				 
			}
			jsonObject.put("msg", "friend");
			jsonObject.put("state", "친구목록조회성공");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
       
 
        
 
        //전체의 JSONObject에 사람이란 name으로 JSON의 정보로 구성된 Array의 value를 입력
        jsonObject.put("friend", userArray);
 
         
       
        return jsonObject;
		
	}
}
