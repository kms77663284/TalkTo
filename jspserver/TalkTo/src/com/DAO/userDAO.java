package com.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.DTO.user;
import com.talkto.conn.ConnectionMariaDB;


public class userDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String sql = "";
	private static userDAO instance;
	
	private userDAO(){}
	
	public static userDAO getInstance() {
		if (instance == null) instance = new userDAO();
		return instance;
	}
	
	public JSONObject loginCheck(String id, String pwd) {
		 JSONObject jsonObject = new JSONObject();
		if(conn == null) {
			conn = ConnectionMariaDB.getConnection();
		}
		try {
			jsonObject.put("msg", "login");
			sql = "SELECT * FROM users WHERE id=? and pwd=PASSWORD(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);;
			pstmt.setString(2, pwd);;
			rs = pstmt.executeQuery();
			if(rs.next()) {
				jsonObject.put("state", "로그인성공");
				JSONObject userInfo = new JSONObject();
				userInfo.put("id", rs.getString("id"));
		        userInfo.put("user_name", rs.getString("user_name"));
		        userInfo.put("phoneNum", rs.getString("phoneNum"));
		        userInfo.put("message", rs.getString("message"));
		        userInfo.put("backgaddress", rs.getString("backgaddress"));
		        userInfo.put("profileaddress", rs.getString("profileaddress"));
		        jsonObject.put("loginInfo", userInfo);
				return jsonObject;
			}else {
				jsonObject.put("state", "로그인실패");
				return jsonObject;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public JSONObject signin(String id,String pwd,String user_name,String phoneNum, String profileaddress,String backgaddress,String message) {
		if(conn == null) {
			conn = ConnectionMariaDB.getConnection();
		}
		//최종 완성될 JSONObject 선언(전체)
        JSONObject jsonObject = new JSONObject();
		try {
			sql = "SELECT id FROM users WHERE id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);;

			rs = pstmt.executeQuery();
			jsonObject.put("msg", "signin");
			if(rs.next()) {
				System.out.println(id +"가 중복입니다.!");
				jsonObject.put("state", "회원가입중복");
				return jsonObject;
			}else {
				sql = "insert into users (id,pwd,user_name,phoneNum,profileaddress,backgaddress,message) VALUES (?,PASSWORD(?),?,?,?,?,?)";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, id);
				pstmt.setString(2, pwd);
				pstmt.setString(3, user_name);
				pstmt.setString(4, phoneNum);
				pstmt.setString(5, profileaddress);
				pstmt.setString(6, backgaddress);
				pstmt.setString(7, message);
				
				pstmt.executeUpdate();
				System.out.println(id +"님의 회원가입 성공!");
				jsonObject.put("state", "회원가입성공");
				return jsonObject;
			}
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("sql 에러!!");
		}
		return null;
	}
	public JSONObject addFriend(String f_code,String id) {
		if(conn == null) {
			conn = ConnectionMariaDB.getConnection();
		}
	
		//최종 완성될 JSONObject 선언(전체)
        JSONObject jsonObject = new JSONObject();
        
		String f_id ="";
		String phoneNum ="";
		if(f_code.substring(0,3).contentEquals("010")) {
			phoneNum =f_code;
			sql = "select * from users where phoneNum='"+phoneNum+"'";
		}else {
			f_id = f_code;
			sql="select * from users where id='"+f_id+"'";
		}
		System.out.println(sql +"sql문");
		System.out.println(id+" "+f_id);
	
		try {
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			jsonObject.put("msg", "addFriend");
			if(rs.next()) {
				jsonObject.put("state", "친구추가성공");
				JSONObject userInfo = new JSONObject();
				userInfo.put("id",rs.getString("id"));
				userInfo.put("backgaddress",rs.getString("backgaddress"));
				userInfo.put("message", rs.getString("message"));
				userInfo.put("phoneNum",rs.getString("phoneNum"));
				userInfo.put("profileaddress",rs.getString("profileaddress"));
				userInfo.put("user_name",rs.getString("user_name"));
				jsonObject.put("friendInfo",userInfo);
				sql="insert into friend (id,f_id) values(?,?)";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, id);
				pstmt.setString(2, f_id);
				pstmt.executeUpdate();
				System.out.println(id+"가"+f_id+"를 친구추가 하였습니다.");
			}else {
				jsonObject.put("state", "친구추가실패");
				System.out.println("친구 추가를 실패했습니다. "+f_id+"라는 아이디나 폰번호가 없습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
		return jsonObject;
	}
}
