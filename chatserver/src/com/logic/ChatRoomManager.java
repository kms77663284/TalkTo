package com.logic;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import com.data.ChatRoom;
import com.data.DAO;
import com.data.User;
import com.data.UserVO;
import com.server.Server;

public class ChatRoomManager {
    private static AtomicInteger atomicInteger; //채팅방 고유값 번호
    private static DAO db = null;
    
    public static void InitRoomnum(int start) {
    	atomicInteger = new AtomicInteger(start);
    }
    
    public static int createRoom(UserVO call, ArrayList<UserVO> list) { // 채팅방 생성
		ChatRoom newChatRoom = new ChatRoom();
		list.add(call);
    	ArrayList<User> ulist = new ArrayList<User>();
		for (UserVO tmp : list) {
			ulist.add(Server.users.get(tmp.getID()));
		}
    	newChatRoom.setUserList(ulist);
    	newChatRoom.setId(atomicInteger.getAndIncrement());
    	
    	Server.roomList.put(newChatRoom.getId(), newChatRoom);
    	
    	if (db == null) db = DAO.getInstance();
    	db.addChatRoom(newChatRoom);
    	
    	return newChatRoom.getId();
    }
    
    public static void deleteRoom(int id) {
    	Server.roomList.remove(id);
    	db.removeChatRoom(id);
    }
    
    public static int roomCount() {
        return Server.roomList.size();
    }
    
    public static ChatRoom getChatRoom(int roomnum) {
    	return Server.roomList.get(roomnum);
    }
}
