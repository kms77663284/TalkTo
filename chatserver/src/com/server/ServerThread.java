package com.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import com.data.ChatRoom;
import com.data.DataPacket;
import com.data.User;
import com.logic.ChatRoomManager;
import com.logic.UserManager;
import com.data.UserVO;
import com.data.DAO;
import com.data.ChatVO;

public class ServerThread extends Thread{
	private Socket socket =null;
	private DataPacket dp = null;
	private DAO db = null;
	ServerThread(Socket socket){
		this.socket = socket;
	}
	
	@Override
    public void run() {
		try {
			ObjectInputStream din = new ObjectInputStream(socket.getInputStream());
        	ObjectOutputStream dos =new ObjectOutputStream(socket.getOutputStream());
        	db = DAO.getInstance();
	        while(true) {
	            try {
					dp = (DataPacket) din.readObject();
				} catch (ClassNotFoundException e) {
					System.out.println("데이터패킷 오브젝트를 받아오지 못했습니다.");
				}catch (SocketException e) {
					System.out.println("소켓에러");
					UserVO outuser = dp.getUdata();
					if (outuser != null) 
						System.out.println(outuser.getID() + "님이 소켓서버에서 퇴장하셨습니다.");
					din.close();
					dos.close();
					socket.close();
					dp =null;
				}
	            
	
	            if( dp == null) {
	                break;
	            }
	
	            switch (dp.getProtocol()) {
		            case MessageProtocol.SOCKET_CONNECT: //클라이언트가 소켓 처음 연결됫을때 자신의 정보를 알려줌
		            	UserManager.addUser(dp.getUdata(), din, dos);
		            	System.out.println(dp.getUdata().getID()+"님이 소켓 서버에 접속했습니다.");
		            	DataPacket data = new DataPacket();
		            	data.setUdata(dp.getUdata());
		            	data.setProtocol(MessageProtocol.SOCKET_CONNECT_SUCCESS);
		            	dos.writeObject(data);
		            	// 채팅 데이터 동기화
		            	System.out.println("동기화 시작");
		            	DataPacket sync = new DataPacket();
		            	sync.setProtocol(MessageProtocol.SYNC_START);
		            	dos.writeObject(sync);
		            	ArrayList<ChatRoom> myroom = db.loadChatRoom(dp.getUdata().getID());
		            	for (ChatRoom tmp : myroom) {
		            		DataPacket crp = new DataPacket();
		            		crp.setProtocol(MessageProtocol.SYNC_CHATROOM);
		            		crp.setRoomId(tmp.getId());
		            		crp.setUserlist(tmp.getJustUsers());
		            		dos.writeObject(crp);
		            		ArrayList<ChatVO> clist = db.loadChat(tmp.getId());
		            		for (ChatVO tmp2 : clist) {
		            			DataPacket cp = new DataPacket();
		            			cp.setProtocol(MessageProtocol.SYNC_CHAT);
		            			cp.setCdata(tmp2);
		            			cp.setRoomId(tmp.getId());
		            			dos.writeObject(cp);
		            		}
		            	}
		            	sync = new DataPacket();
		            	sync.setProtocol(MessageProtocol.SYNC_DONE);
		            	dos.writeObject(sync);
		            	break;
		            case MessageProtocol.CHAT_CREATE: //채팅방 생성
		            	// 메세지를 받은 유저들은 db에 채팅방을 생성
		            	int check = db.checkChatRoom(dp.getUdata(), dp.getUserlist());
		            	if (check >= 0) {
		            		DataPacket justin = new DataPacket();
		            		justin.setRoomId(check);
		            		justin.setProtocol(MessageProtocol.CHAT_IN);
		            		dp.getUserlist().add(dp.getUdata());
		            		justin.setUserlist(dp.getUserlist());
		            		dos.writeObject(justin);
		            	}else {
		            		int roomnum = ChatRoomManager.createRoom(dp.getUdata(), dp.getUserlist());
		            		ChatRoomManager.getChatRoom(roomnum).Notify();
		            	}
		            	break;
		            case MessageProtocol.CS_SEND_MSG: // 클라이언트가 메세지를 보냄
		            	// 서버는 모든 채팅방 유저에게 메세지를 전달. 본인도 포함
		            	ChatRoomManager.getChatRoom(dp.getRoomId()).broadcast(dp);
		            	break;
		            case MessageProtocol.CHAT_OUT: // 클라이언트가 채팅방을 나감
		            	ChatRoomManager.getChatRoom(dp.getRoomId()).chatout(dp);
		            	break;
					default:
						break;
				}
	        }
	    } catch(IOException e) {
	        try {
				socket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	        e.printStackTrace();
	    }
	}
}
