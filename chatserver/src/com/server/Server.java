package com.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import com.data.ChatRoom;
import com.data.DAO;
import com.data.User;
import com.logic.ChatRoomManager;



public class Server {
	public static HashMap<String, User> users = new HashMap<String, User>();
	public static HashMap<Integer, ChatRoom> roomList = new HashMap<Integer, ChatRoom>();

	private static final int PORT = 5000;
	
	public static void main(String[] args) {
		DAO db = DAO.getInstance();
		db.initServer();
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket();
	
	        String hostAddress = InetAddress.getLocalHost().getHostAddress();
	        serverSocket.bind( new InetSocketAddress(hostAddress, PORT) );
	        System.out.println("연결 기다리는중 - " + hostAddress + ":" + PORT);
	        
	        while(true) {
	            Socket socket = serverSocket.accept();
	            new ServerThread(socket).start();;
	        }
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
            try {
                if( serverSocket != null && !serverSocket.isClosed() ) {
                	serverSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}
}
