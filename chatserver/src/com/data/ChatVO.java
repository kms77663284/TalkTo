package com.data;

import java.io.Serializable;

public class ChatVO implements Serializable {
    private UserVO user;
    private String content;
    private String time;
    private boolean me;
    private boolean tag;
    private int cid;

    public int getCID(){
        return cid;
    }

    public void setCID(int cid){
        this.cid = cid;
    }
    
    public boolean isTag() {
        return tag;
    }

    public void setTag(boolean tag) {
        this.tag = tag;
    }

    public boolean isMe() {
        return me;
    }

    public void setMe(boolean me) {
        this.me = me;
    }

    public UserVO getUser() {
        return user;
    }

    public void setUser(UserVO user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
