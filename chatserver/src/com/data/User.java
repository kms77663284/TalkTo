package com.data;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class User {
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private UserVO udata;
	public User() {}
	public User(UserVO data, ObjectOutputStream oos, ObjectInputStream ois) {
		udata = data;
		this.oos = oos;
		this.ois = ois;
	}
	
	
	public ObjectInputStream getOis() {
		return ois;
	}
	public void setOis(ObjectInputStream ois) {
		this.ois = ois;
	}
	public ObjectOutputStream getOos() {
		return oos;
	}
	public void setOos(ObjectOutputStream oos) {
		this.oos = oos;
	}
	public UserVO getUdata() {
		return udata;
	}
	public void setUdata(UserVO udata) {
		this.udata = udata;
	}
}
